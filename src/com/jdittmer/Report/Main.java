package com.jdittmer.Report;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.jdittmer.Report.GUI;
import com.jdittmer.Report.MySQL.*;

public class Main extends JavaPlugin implements Listener {
	
	private static Plugin plugin;
	
	public static FileConfiguration config;
	File cfile;
	
	public void onEnable() {
		plugin = this;
		
		/* config.yml LOADING */
		config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		cfile = new File(getDataFolder(), "config.yml");
		System.out.println("[Report]"+SOPANSIColors.GREEN+" config.yml loaded!"+SOPANSIColors.RESET);
		
		if(MySQL.checkConnection() == true){
			MySQL.createReportsTable();
			Language.createLangTable();
			Language.createVariables();
			
			/* TEST CONNECTION TO DATABASE [start] */
			Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
				public void run(){
					try {
						MySQL.conn = DriverManager.getConnection("jdbc:mysql://" + MySQL.dbHost + ":" + MySQL.dbPort + "/" + MySQL.dbName + "?" + "user=" + MySQL.dbUser + "&" + "password=" + MySQL.dbPassword + "&useSSL=" + MySQL.useSSL);
						System.out.println("[Report]"+SOPANSIColors.GREEN+" Task DB: Test connection established!"+SOPANSIColors.RESET);
					} catch (SQLException e) {
						System.out.println("[Report]"+SOPANSIColors.RED+" Task DB: Test connection failed!"+SOPANSIColors.RESET);
						System.out.println("[Report]"+SOPANSIColors.RED+" Task DB: Please edit the config.yml and check your database connection!"+SOPANSIColors.RESET);
					}
				}
			}, 0L, 12000L);
			/* TEST CONNECTION TO DATABASE [end] */
			
			Bukkit.getServer().getPluginManager().registerEvents(this, this);
			Bukkit.getPluginManager().registerEvents(new GUI(), this);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		boolean pluginIsOnline = true;
		
		/* CHECK DATABASE CONNECTION */
		if(p.isOp()){
			if(MySQL.checkConnection() == false){
				pluginIsOnline = false;
				p.sendMessage(Main.plgChatStart());
				p.sendMessage(Main.pluginTag() + ChatColor.RED + "DB: No connection to database!");
				p.sendMessage(Main.pluginTag() + ChatColor.RED + "DB: Please edit the config.yml and check your database connection!");
				p.sendMessage(Main.plgChatEnd());
				Bukkit.getPluginManager().disablePlugin(this);
			}else if(MySQL.checkConnection() == true){
				pluginIsOnline = true;
			}
		}
		
		if(pluginIsOnline == true){
			/* CHECK IF USE THE ACTUALLY PLUGIN VERSION */
			if(p.isOp()){
				CheckVersion.checkVersion(p.getName());
				p.sendMessage("");
			}
			
			/* CHECK HOW MUCH REPORTS ARE OPEN */
			if(p.hasPermission("report.admin.book")){
				CheckReports.checkNumReports(p.getName());
				p.sendMessage("");
			}
		}
	}
	
	public static Plugin getPlugin() {
		return plugin;
	}
	
	public static long timestamp(){
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long timeStamp = timestamp.getTime() / 1000;
		
		return timeStamp;
	}
	
	public static String timeDate(){
    	Date today1 = Calendar.getInstance().getTime();
		DateFormat tm = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		String timeDate = tm.format(today1);
		
		return timeDate;
	}
	
	public static String timeMonth(){
    	Date today1 = Calendar.getInstance().getTime();
		DateFormat tm = new SimpleDateFormat("MMMMM");
		String timeMonth = tm.format(today1);
		
		return timeMonth;
	}
	
	public static String timestampToDate(long timestamp){
		Timestamp timestamp2 = new Timestamp(timestamp * 1000);
		Date date = new Date(timestamp2.getTime());
		
		if(config.getInt("dateFormat") == 24){
			DateFormat tm = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			return tm.format(date);
		}else if(config.getInt("dateFormat") == 12){
			DateFormat tm = new SimpleDateFormat("MM/dd/yyyy KK:mm:ssa");
			return tm.format(date);
		}
		
		return "Error config dateFormat";
	}
	
	public static Date tbExpiresTime(int time, String type){
		long currentTimeMillis = System.currentTimeMillis();
		long banTime = 0;
		
		if(type.equals("s")){
			banTime = time*1000;
			return new Date(currentTimeMillis+banTime);
		}else if(type.equals("m")){
			banTime = 60*time*1000;
			return new Date(currentTimeMillis+banTime);
		}else if(type.equals("h")){
			banTime = 60*60*time*1000;
			return new Date(currentTimeMillis+banTime);
		}else if(type.equals("d")){
			banTime = 60*60*24*time*1000;
			return new Date(currentTimeMillis+banTime);
		}else if(type.equals("mo")){
			banTime = 60*60*24*time*1000;
			banTime = banTime*30;
			return new Date(currentTimeMillis+banTime);
		}else if(type.equals("y")){
			banTime = 60*60*24*time*1000;
			banTime = banTime*365;
			System.out.println("y:"+currentTimeMillis+" + "+banTime);
			return new Date(currentTimeMillis+banTime);
		}
		
		return null;
	}

	public static String plgChatStart(){
		return ChatColor.GOLD + "--------------------- " + ChatColor.BOLD + "[Report]" + ChatColor.BOLD + " --------------------";
	}
	
	public static String plgChatEnd(){
		return ChatColor.GOLD + "-------------------------------------------------------";
	}
	
	public static String noPermissions(){
		return Language.getLang("Miscellaneous.no-permission");
	}
	
	public static String pluginVersion(){
		return Bukkit.getPluginManager().getPlugin("Report").getDescription().getVersion();
	}
	
	public static String pluginTag(){
		return ChatColor.GOLD + "[Report] ";
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("report")){
			if(args.length == 0){
				if(sender.hasPermission("report.report")){
					if(!(sender instanceof Player)){
						sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.only-players-exec-cmd"));
						return true;
					}
					
					sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.report-cmd"));
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}else{
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("ver")) {
						if(sender.hasPermission("report.ver")){
							sender.sendMessage(plgChatStart());
							sender.sendMessage(ChatColor.RED + "== VERSION");
							sender.sendMessage(ChatColor.GOLD + "Version: " + ChatColor.WHITE + this.getDescription().getVersion());
							sender.sendMessage("");
							sender.sendMessage(ChatColor.GOLD + "Written by: " + ChatColor.WHITE + "Justin Dittmer (JND_3004)");
							sender.sendMessage(ChatColor.GOLD + "Website: " + ChatColor.WHITE + "http://jdittmer.com");
							sender.sendMessage(ChatColor.GOLD + "Spigot: " + ChatColor.WHITE + "https://www.spigotmc.org/members/jnd_3004.282948/");
							sender.sendMessage(ChatColor.GOLD + "GitLab: " + ChatColor.WHITE + "https://gitlab.com/JND_3004");
							sender.sendMessage("");
							sender.sendMessage(ChatColor.GOLD + "Found a bug? Send a mail: " + ChatColor.WHITE + "incoming+JND_3004/Report@gitlab.com");
							sender.sendMessage(plgChatEnd());
	    	    			return true;
						}else{
							sender.sendMessage(pluginTag() + noPermissions());
	    	    			return true;
						}
	    			}
    			}
				
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("help")){
						if(sender.hasPermission("report.help")){
							sender.sendMessage(plgChatStart());
							if(sender.hasPermission("report.report")){
								sender.sendMessage(Language.getLang("Main.helpList.player-commands"));
								sender.sendMessage(Language.getLang("Main.helpList.open-the-help"));
								sender.sendMessage(Language.getLang("Main.helpList.show-the-version"));
								sender.sendMessage(Language.getLang("Main.helpList.report-a-player"));
							}
							if(sender.hasPermission("report.admin")){
								sender.sendMessage("");
								sender.sendMessage(Language.getLang("Main.helpList.admin-commands"));
								sender.sendMessage(Language.getLang("Main.helpList.ban-a-player"));
								sender.sendMessage(Language.getLang("Main.helpList.tempban-a-player"));
								sender.sendMessage(Language.getLang("Main.helpList.unban-a-player"));
								sender.sendMessage(Language.getLang("Main.helpList.kick-a-player"));
								sender.sendMessage("");
								sender.sendMessage(Language.getLang("Main.helpList.open-report-book"));
								sender.sendMessage(Language.getLang("Main.helpList.accept-report"));
								sender.sendMessage(Language.getLang("Main.helpList.delete-report"));
								sender.sendMessage("");
								sender.sendMessage(Language.getLang("Main.helpList.update-lang-db"));
								sender.sendMessage(Language.getLang("Main.helpList.reload-config"));
							}
							sender.sendMessage(plgChatEnd());
							return true;
						}
					}
				}
				
				if(args.length == 1){
    	    		if(args[0].equalsIgnoreCase("reload")) {
    	    			if(sender.hasPermission("report.admin.reload")){
	    	    			config = YamlConfiguration.loadConfiguration(cfile);
	    	    			sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.reload-message"));
	    	    			
	    	    			/* TEST CONNECTION TO DATABASE [start] */
	    	    			try {
	    						MySQL.conn = DriverManager.getConnection("jdbc:mysql://" + MySQL.dbHost + ":" + MySQL.dbPort + "/" + MySQL.dbName + "?" + "user=" + MySQL.dbUser + "&" + "password=" + MySQL.dbPassword + "&useSSL=" + MySQL.useSSL);
	    						System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: Test connection established!"+SOPANSIColors.RESET);
	    						sender.sendMessage(pluginTag() + ChatColor.GREEN + "Database: Test connection established!");
	    					} catch (SQLException e) {
	    						System.out.println("[Report]"+SOPANSIColors.RED+" DB: Test connection failed!"+SOPANSIColors.RESET);
	    						System.out.println("[Report]"+SOPANSIColors.RED+" DB: Please edit the config.yml and check your database connection!"+SOPANSIColors.RESET);
	    						sender.sendMessage(pluginTag() + ChatColor.RED + "Database: Test connection failed!");
	    						sender.sendMessage(pluginTag() + ChatColor.RED + "Database: Please edit the config.yml and check your database connection!");
	    					}
	    	    			/* TEST CONNECTION TO DATABASE [end] */
	    	    			
	    	    			return true;
    	    			}else{
							sender.sendMessage(pluginTag() + noPermissions());
	    	    			return true;
						}
    	    		}
    			}
				
				/* REPORT PLAYER */
				if(args.length >= 2){
					if(sender.hasPermission("report.report")){
						if(!(sender instanceof Player)){
							sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.only-players-exec-cmd"));
							return true;
						}
						
						Player p = (Player) sender;
						
						long report_date = timestamp();
						String from_user = p.getName();
						String report_user = args[0];
						
						String report_reason = "";
				        for (int i = 1; i < args.length;) {
				        	report_reason += args[i] + " ";
				        	i++;
				        }
				        report_reason = report_reason.trim();
						
				        SaveReport.addUserReport(sender, report_date, from_user, report_user, report_reason);
				        return true;
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
    	    			return true;
					}
				}
				
				/* OPEN REPORT BOOK */
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("book")){
						if(!(sender instanceof Player)){
							sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.only-players-exec-cmd"));
							return true;
						}
						
						if(sender.hasPermission("report.admin.book")){
							Player p = (Player) sender;
							GUI.openGUI(p, 1, 0, 53);
						}else{
							sender.sendMessage(pluginTag() + noPermissions());
	    	    			return true;
						}
					}
				}
			}
		}
		
		/* UPDATE LANGUAGE DB */
		if(cmd.getName().equalsIgnoreCase("rupdate")){
			if(args.length == 1){
				if(args[0].equalsIgnoreCase("lang")){
					if(sender.hasPermission("report.admin.updatedb")){
						Language.updateLang(sender);
						return true;
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
		    			return true;
					}
				}
			}
		}
		
		/* ACCEPT PLAYER REPORT */
		if(cmd.getName().equalsIgnoreCase("raccept")){
			if(!(sender instanceof Player)){
				sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.only-players-exec-cmd"));
				return true;
			}
			
			if(args.length == 1){
				if(sender.hasPermission("report.admin.report.accept")){
					AcceptReport.acceptUserReport(sender, args[0], sender.getName());
					return true;
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}
		}
		
		/* DELETE PLAYER REPORT */
		if(cmd.getName().equalsIgnoreCase("rdelete")){
			if(!(sender instanceof Player)){
				sender.sendMessage(pluginTag() + Language.getLang("Main.helpList.only-players-exec-cmd"));
				return true;
			}
			
			if(args.length == 1){
				if(sender.hasPermission("report.admin.report.delete")){
					DeleteReport.deleteUserReport(sender, args[0]);
					return true;
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}
		}
		
		/* BAN PLAYER */
		if(cmd.getName().equalsIgnoreCase("b")){
			if(args.length < 2){
				if(sender.hasPermission("report.admin.ban")){
					sender.sendMessage(pluginTag() + Language.getLang("BanCMD.cmd"));
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}else{
				if(args.length >= 2){
					if(sender.hasPermission("report.admin.ban")){
						
						String ban_player = args[0];
						String ban_reason = "";
				        for (int i = 1; i < args.length;) {
				        	ban_reason += args[i] + " ";
				        	i++;
				        }
				        ban_reason = ban_reason.trim();
				        
				        Bukkit.getBanList(Type.NAME).addBan(ban_player, ban_reason, null, sender.getName());
				        sender.sendMessage(pluginTag() + Language.getLang("BanCMD.the-player") + ban_player + Language.getLang("BanCMD.was-banned"));
				        System.out.println("[Report]"+SOPANSIColors.RED+" "+ ban_player +" was banned by "+ sender.getName() +SOPANSIColors.RESET);
				        return true;
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
		    			return true;
					}
				}else{
					sender.sendMessage(pluginTag() + Language.getLang("BanCMD.cmd"));
					return true;
				}
			}
		}
		
		/* TEMPBAN PLAYER */
		if(cmd.getName().equalsIgnoreCase("tb")){
			if(args.length < 3){
				if(sender.hasPermission("report.admin.tempban")){
					sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.cmd"));
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}else{
				if(args.length >= 3){
					if(sender.hasPermission("report.admin.tempban")){
						
						String tempban_player = args[0];
						String tempban_time[] = args[1].split("(?<=[0-9])(?=[a-zA-Z])");
						String tempban_reason = "";
				        for (int i = 2; i < args.length;) {
				        	tempban_reason += args[i] + " ";
				        	i++;
				        }
				        tempban_reason = tempban_reason.trim();
				        
				        if(Integer.parseInt(tempban_time[0]) > 0){
					        Bukkit.getBanList(Type.NAME).addBan(tempban_player, tempban_reason, tbExpiresTime(Integer.parseInt(tempban_time[0]), tempban_time[1]), sender.getName());
					        sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.the-player") + tempban_player + Language.getLang("TempbanCMD.was-banned"));
					        System.out.println("[Report]"+SOPANSIColors.RED+" "+ tempban_player +" was temporary banned by "+ sender.getName() +SOPANSIColors.RESET);
					        return true;
				        }
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
		    			return true;
					}
				}else{
					sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.cmd"));
					return true;
				}
			}
		}
		
		/* UNBAN PLAYER */
		if(cmd.getName().equalsIgnoreCase("ub")){
			if(args.length < 1){
				if(sender.hasPermission("report.admin.unban")){
					sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.cmd"));
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}else{
				if(args.length == 1){
					if(sender.hasPermission("report.admin.unban")){
						String unban_player = args[0];
						
						Bukkit.getBanList(Type.NAME).pardon(unban_player);
				        sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.the-player") + unban_player + Language.getLang("UnbanCMD.was-unban"));
				        System.out.println("[Report]"+SOPANSIColors.GREEN+" "+ unban_player +" was unbanned by "+ sender.getName() +SOPANSIColors.RESET);
				        return true;
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
		    			return true;
					}
				}else{
					sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.cmd"));
					return true;
				}
			}
		}
		
		/* KICK PLAYER */
		if(cmd.getName().equalsIgnoreCase("k")){
			if(args.length < 2){
				if(sender.hasPermission("report.admin.kick")){
					sender.sendMessage(pluginTag() + Language.getLang("KickCMD.cmd"));
				}else{
					sender.sendMessage(pluginTag() + noPermissions());
	    			return true;
				}
			}else{
				if(args.length >= 2){
					if(sender.hasPermission("report.admin.kick")){
						Player kick_player = Bukkit.getPlayer(args[0]);
						
						if(kick_player != null && kick_player.isOnline()){
							String kick_reason = "";
					        for (int i = 1; i < args.length;) {
					        	kick_reason += args[i] + " ";
					        	i++;
					        }
					        kick_reason = kick_reason.trim();
					        
					        kick_player.kickPlayer(kick_reason);
					        sender.sendMessage(pluginTag() + Language.getLang("KickCMD.the-player") + kick_player + Language.getLang("KickCMD.was-kicked"));
					        System.out.println("[Report]"+SOPANSIColors.RED+" "+ kick_player +" was kicked by "+ sender.getName() +SOPANSIColors.RESET);
					        return true;
						}else if(kick_player == null){
							sender.sendMessage(pluginTag() + Language.getLang("KickCMD.the-player2") + args[0] + Language.getLang("KickCMD.is-not-online"));
							return true;
						}
					}else{
						sender.sendMessage(pluginTag() + noPermissions());
		    			return true;
					}
				}else{
					sender.sendMessage(pluginTag() + Language.getLang("KickCMD.cmd"));
					return true;
				}
			}
		}
		
		return true;
	}
}