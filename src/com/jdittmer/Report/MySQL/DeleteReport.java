package com.jdittmer.Report.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.jdittmer.Report.Main;

public class DeleteReport {
	public static boolean deleteUserReport(CommandSender sender, String reportID) {
		MySQL.conn = MySQL.getInstance();
		
		if(MySQL.conn != null) {
	    	try {
	    		PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE report_id = ?");
				query.setString(1, reportID);
				ResultSet result = query.executeQuery();
	    		
	    		if(result.next()){
	    			PreparedStatement sql = MySQL.conn.prepareStatement("DELETE FROM report_reports WHERE report_id = ?");
	    			sql.setString(1, reportID);
					sql.executeUpdate();
	    			
	    			sender.sendMessage(Main.pluginTag() + Language.getLang("DeleteReport.the-report-id") + reportID + Language.getLang("DeleteReport.was-removed"));
	    		}else{
	    			sender.sendMessage(Main.pluginTag() + Language.getLang("DeleteReport.the-report-id") + reportID + Language.getLang("DeleteReport.was-not-found-in-db"));
	    			return false;
	    		}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		sender.sendMessage(Main.pluginTag() + ChatColor.RED + "Error: \"MySQL.DeletePlayerReport.java\" - Please contact the server admin!");
	    		sender.sendMessage(Main.pluginTag() + ChatColor.RED + "Error: " + e.getMessage());
	    	}
    	}
		return true;
	}
}