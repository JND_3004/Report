package com.jdittmer.Report.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.jdittmer.Report.Main;

public class CheckReports {
	public static boolean checkNumReports(String user_name){
		MySQL.conn = MySQL.getInstance();
		if(MySQL.conn != null) {
			try {
				Player player = Bukkit.getPlayer(user_name);
				
				PreparedStatement query = MySQL.conn.prepareStatement("SELECT COUNT(*) FROM report_reports WHERE agent = 0");
				ResultSet result = query.executeQuery();
				
				result.first();
				int rows = result.getInt("COUNT(*)");
								
				if(rows == 1){
					player.sendMessage(Main.plgChatStart());
					player.sendMessage(Main.pluginTag() + Language.getLang("CheckReports.1report-are-open"));
					player.sendMessage(Main.pluginTag() + Language.getLang("CheckReports.use-book-see-reports"));
					player.sendMessage(Main.plgChatEnd());
				}else if(rows > 1){
					player.sendMessage(Main.plgChatStart());
					player.sendMessage(Main.pluginTag() + Language.getLang("CheckReports.there-are-currently") + rows + Language.getLang("CheckReports.reports-open"));
					player.sendMessage(Main.pluginTag() + Language.getLang("CheckReports.use-book-see-reports"));
					player.sendMessage(Main.plgChatEnd());
				}else if(rows == 0){
					player.sendMessage(Main.pluginTag() + Language.getLang("CheckReports.no-reports-available"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}