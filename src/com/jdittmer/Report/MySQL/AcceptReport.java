package com.jdittmer.Report.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.jdittmer.Report.Main;

public class AcceptReport {
	public static boolean acceptUserReport(CommandSender sender, String reportID, String agent_name) {
		MySQL.conn = MySQL.getInstance();
 
	    if(MySQL.conn != null) {
	    	try {
	    		PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE report_id = ?");
				query.setString(1, reportID);
				ResultSet result = query.executeQuery();
				
	    		if(result.next()){
	    			PreparedStatement sql = MySQL.conn.prepareStatement("UPDATE report_reports SET agent = 1, agent_name = ? WHERE report_id = ?");
	    			sql.setString(1, agent_name);
	    			sql.setString(2, reportID);
					sql.executeUpdate();
	    			sender.sendMessage(Main.pluginTag() + Language.getLang("AcceptReport.accept-report"));
				}else{
					sender.sendMessage(Main.pluginTag() + Language.getLang("AcceptReport.no-report"));
					return false;
				}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		sender.sendMessage(Main.pluginTag() + ChatColor.RED + "Error: \"MySQL.AcceptPlayerReport.java\" - Please contact the server admin!");
	    		sender.sendMessage(Main.pluginTag() + ChatColor.RED + "Error: " + e.getMessage());
	    	}
    	}
	    return true;
	}
}