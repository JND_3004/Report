package com.jdittmer.Report;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import com.jdittmer.Report.Utils.ItemUtil;
import com.jdittmer.Report.MySQL.Language;
import com.jdittmer.Report.MySQL.MySQL;

public class GUI implements Listener {
	public static void openGUI(Player p, int site, int limit1, int limit2){
        Inventory inv = Bukkit.createInventory(null, 54, Language.getLang("GUI.all-reports")+" - "+site);
       
        /* MYSQL - LISTE ALLE REPORTS AUF [start] */
        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            try {
                PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports ORDER BY `date` DESC LIMIT ?,?");
                query.setInt(1, limit1);
                query.setInt(2, limit2);
                ResultSet result = query.executeQuery();
               
                int item_pos = 0;
               
                while (result.next()) {
                    int report_id = result.getInt("report_id");
                    long report_date = result.getLong("date");
                    String report_user = result.getString("report_user");
                    String from_user = result.getString("from_user");
                    String report_reason = result.getString("report_reason");
                    int agent = result.getInt("agent");
                    String agent_name = result.getString("agent_name");
                   
                    if(agent == 0){
                        inv.setItem(item_pos, ItemUtil.create("Report ID: "+ report_id, Material.BOOK, Language.getLang("GUI.report-date") + Main.timestampToDate(report_date), Language.getLang("GUI.reported-user") + report_user, Language.getLang("GUI.reported-by") + from_user, Language.getLang("GUI.reason") + report_reason, "", Language.getLang("GUI.agent") + Language.getLang("GUI.agent-no"), null, 1));
                    }else if(agent == 1){
                        inv.setItem(item_pos, ItemUtil.create("Report ID: "+ report_id, Material.BOOK_AND_QUILL, Language.getLang("GUI.report-date") + Main.timestampToDate(report_date), Language.getLang("GUI.reported-user") + report_user, Language.getLang("GUI.reported-by") + from_user, Language.getLang("GUI.reason") + report_reason, "", Language.getLang("GUI.agent") + Language.getLang("GUI.agent-yes"), Language.getLang("GUI.agent-name") + agent_name, 1));
                    }
                    
                    if(site >= 2){
                    	inv.setItem(45, ItemUtil.create(Language.getLang("GUI.site") + (site - 1), Material.BUCKET, Language.getLang("GUI.backward"), null, null, null, null, null, null, 1));
                    	if(item_pos == 44){
                            item_pos++;
                        }
                    }
                   
                    if(item_pos == 52){
                        inv.setItem(53, ItemUtil.create(Language.getLang("GUI.site") + (site + 1), Material.LAVA_BUCKET, Language.getLang("GUI.forward"), null, null, null, null, null, null, 1));
                        break;
                    }
                   
                    item_pos++;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        /* MYSQL - LISTE ALLE REPORTS AUF [end] */
        
        Bukkit.getScheduler().runTaskLater(Main.getPlugin(), new Runnable() {
            @Override
            public void run() {
                p.openInventory(inv);
            }
        }, 1L);
    }
   
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
       
        int site = Integer.parseInt(e.getInventory().getName().replace(Language.getLang("GUI.all-reports")+" - ", ""));
       
        if(e.getInventory().getName().equalsIgnoreCase(Language.getLang("GUI.all-reports")+" - "+site)){
            e.setCancelled(true);
 
            if(e.getCurrentItem() == null){
                p.closeInventory();
            }
           
           
            /* SEITE VORW�RTS */
            if(e.getCurrentItem().getType() == Material.LAVA_BUCKET){
            	site = site+1;
               
                int limit = 0;
                int limit2 = 0;
                if(site == 1){
                    limit = 0;
                    limit2 = 53;
                }else if(site == 2){
                    limit = 53;
                    limit2 = 52;
                }else if(site > 2){
                    limit = 53*(site-1)-(site-2);
                    limit2 = 52;
                }
               
                openGUI(p, site, limit, limit2);
            }
            
            
            /* SEITE ZUR�CK */
            if(e.getCurrentItem().getType() == Material.BUCKET){
                site = site-1;
               
                int limit = 0;
                int limit2 = 0;
                if(site == 1){
                    limit = 0;
                    limit2 = 53;
                }else if(site == 2){
                    limit = 53;
                    limit2 = 52;
                }else if(site > 2){
                    limit = 53*(site-1)-(site-2);
                    limit2 = 52;
                }
               
                openGUI(p, site, limit, limit2);
            }
			
			
			/* FUNKTION FALLS REPORT HAT EINEN AGENT */
			if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL){
				String reportID = e.getCurrentItem().getItemMeta().getDisplayName().replace("Report ID: ", "");
				
				/* MYSQL - ZUM REPORT TELEPORTIEREN, NACH KLICK AUF BUCH [start] */
				MySQL.conn = MySQL.getInstance();
				if(MySQL.conn != null) {
					try {
						PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE report_id = ?");
						query.setString(1, reportID);
						ResultSet result = query.executeQuery();
		 
						while (result.next()) {
							World report_user_world = Bukkit.getServer().getWorld(result.getString("report_user_world"));
							int report_user_x = result.getInt("report_user_x");
							int report_user_y = result.getInt("report_user_y");
							int report_user_z = result.getInt("report_user_z");
							float report_user_yaw = result.getFloat("report_user_yaw");
							float report_user_pitch = result.getFloat("report_user_pitch");
																	
							p.teleport(new Location(report_user_world, report_user_x, report_user_y, report_user_z, report_user_yaw, report_user_pitch));
							p.sendMessage(Main.pluginTag() + ChatColor.GREEN + "/raccept " + reportID + Language.getLang("GUI.or") + "/rdelete " + reportID);
						}
					} catch (SQLException sqle) {
						sqle.printStackTrace();
					}
				}
				/* MYSQL - ZUM REPORT TELEPORTIEREN, NACH KLICK AUF BUCH [end] */
					
				p.closeInventory();
			}else{
				
				/* FUNKTION FALLS REPORT HAT ++ NOCH KEINEN ++ AGENT */
				if (e.getCurrentItem().getType() == Material.BOOK){
					String reportID = e.getCurrentItem().getItemMeta().getDisplayName().replace("Report ID: ", "");
					
					/* MYSQL - ZUM REPORT TELEPORTIEREN, NACH KLICK AUF BUCH [start] */
					MySQL.conn = MySQL.getInstance();
					if(MySQL.conn != null) {
						try {
							PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE report_id = ?");
							query.setString(1, reportID);
							ResultSet result = query.executeQuery();
			 
							while (result.next()) {
								World report_user_world = Bukkit.getServer().getWorld(result.getString("report_user_world"));
								int report_user_x = result.getInt("report_user_x");
								int report_user_y = result.getInt("report_user_y");
								int report_user_z = result.getInt("report_user_z");
								float report_user_yaw = result.getFloat("report_user_yaw");
								float report_user_pitch = result.getFloat("report_user_pitch");
																		
								p.teleport(new Location(report_user_world, report_user_x, report_user_y, report_user_z, report_user_yaw, report_user_pitch));
								p.sendMessage(Main.pluginTag() + ChatColor.GREEN + "/raccept " + reportID + Language.getLang("GUI.or") + "/rdelete " + reportID);
							}
						} catch (SQLException sqle) {
							sqle.printStackTrace();
						}
					}
					/* MYSQL - ZUM REPORT TELEPORTIEREN, NACH KLICK AUF BUCH [end] */
				}

				p.closeInventory();
			}
		}
	}
}