package com.jdittmer.Report;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.jdittmer.Report.MySQL.Language;

import net.md_5.bungee.api.ChatColor;

public class CheckVersion {
	public static void checkVersion(String user_name){
		
		Player player = Bukkit.getPlayer(user_name);
		
		try {
			URLConnection connection = new URL("https://gitlab.com/JND_3004/Report/raw/master/versions/version.txt").openConnection();
			connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.connect();

			BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				sb.append(line);
			}
			String value = sb.toString();

			if(!value.equalsIgnoreCase(Main.pluginVersion())){
				player.sendMessage(Main.plgChatStart());
				player.sendMessage(Main.pluginTag() + Language.getLang("CheckVersion.you-use-a-old-version"));
				player.sendMessage(Main.pluginTag() + Language.getLang("CheckVersion.please-dl-new-version") + value + Language.getLang("CheckVersion.and-reload-server"));
				player.sendMessage(Main.pluginTag() + ChatColor.GRAY + "https://www.spigotmc.org/resources/report.54161/history");
				player.sendMessage(Main.plgChatEnd());
			}
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}