# What is Report?
Report is a plugin that allows you to report, ban and kick players.
* Report requires **access to a database**.

# Functions
* Ban evil players (**/b &lt;player&gt; &lt;reason&gt;**)
* Ban evil players temporary (**/tb &lt;player&gt; &lt;[time]s|m|h|d|mo|y&gt; &lt;reason&gt;**)
* Unban a player (**/ub &lt;player&gt;**)
* Kick a player (**/k &lt;player&gt; &lt;reason&gt;**)
* Report a player (**/report &lt;player&gt; &lt;reason&gt;**)
* Open the book for all reports (**/report book**)
* Find **informations** about the report (move mouse on the book)
* **Teleport** to the report (click on the book)
* **Accept/Delete** player reports
* **Spam protection** > You can only report the same person every 30 minutes
* No own report > You **can't report yourself**

# Others
*  Commands and Permissions > [Wiki](https://gitlab.com/JND_3004/Report/wikis/Commands-and-Permissions)
*  Download history > [Wiki](https://gitlab.com/JND_3004/Report/wikis/Download-history)

# How to ..
*  download and install > [Wiki](https://gitlab.com/JND_3004/Report/wikis/How-to-..-download-and-install)
*  configure the config.yml > [Wiki](https://gitlab.com/JND_3004/Report/wikis/How-to-..-configure-the-config.yml)